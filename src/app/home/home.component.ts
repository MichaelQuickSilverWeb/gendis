import { Component, OnInit} from '@angular/core';
import { DataService } from '../data-api.service';
import { ClarityModule } from '@clr/angular';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

  statusD: Object;
  statusSum: Object;
  status50: Object;
  statusUn: Object;

  constructor(private data: DataService) {}
  ngOnInit() {
    this.data.getData().subscribe(data => {
        this.statusD = data;
        console.log(this.statusD);
      }
    );
    this.data.getDataSum().subscribe(data => {
        this.statusSum = data;
        console.log(this.statusSum);
      }
    );
    this.data.getData50().subscribe(data => {
        this.status50 = data;
        console.log(this.status50);
      }
      );
    this.data.getDataUn().subscribe(data => {
        this.statusUn = data;
        console.log(this.statusUn);
      }
      );
    }
}

