import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private http: HttpClient) { }
   getData() {
        return this.http.get('https://bqlf8qjztdtr.statuspage.io/api/v2/status.json');
    }
   getDataSum() {
         return this.http.get('https://bqlf8qjztdtr.statuspage.io/api/v2/summary.json');
  }
  getData50() {
         return this.http.get('https://bqlf8qjztdtr.statuspage.io/api/v2/incidents.json');
  }
  getDataUn() {
         return this.http.get('https://bqlf8qjztdtr.statuspage.io/api/v2/incidents/unresolved.json');
  }
  }
